export const dummyCars = [
  {
    id: 1,
    name: 'Toyota C-HR',
    brand: 'Toyota',
    year: 2020,
    image: require('../assets/images/toyota-chr.jpeg'),
    value: 'Toyota C-HR',
    backgroundColor: '#3d3d3d',
    fontColor: '#fff',
  },
  {
    id: 2,
    name: 'Honda Civic',
    brand: 'Honda',
    year: 2020,
    image: require('../assets/images/honda-civic.jpg'),
    value: 'Honda Civic',
    backgroundColor: '#fff',
    fontColor: '#222',
  },
  {
    id: 3,
    name: 'Mazda CX-3',
    brand: 'Mazda',
    year: 2020,
    image: require('../assets/images/mazda-cx3.jpg'),
    value: 'Mazda CX-3',
    backgroundColor: '#CB4F40',
    fontColor: '#fff',
  },
];

export const getRandomCar = () => {
  const randomIdx = Math.floor(Math.random() * Math.floor(3));

  return dummyCars[randomIdx];
}
