import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios'; // npm install axios
import {ProspectItem} from '../components/ProspectItem';
import {FlatList} from 'react-native-gesture-handler';
import {dummyProspects} from '../dummyDatas/prospects';
import {getRandomCar} from '../dummyDatas/cars'

const styles = StyleSheet.create({
  topButton: {
    backgroundColor: 'hsl(0, 0%, 90%)',
    width: '45%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'hsl(0, 0%, 0%)',
    borderRadius: 5,
  },
  textLg: {
    color: '#000',
    fontSize: 18,
  },
  topButtonWrapper: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginVertical: 16,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  separator: {
    marginBottom: 10,
  },
  listContainer: {
    flex: 1,
    paddingHorizontal: 12,
  },
});

const API_URL = 'https://5f1500684693a6001627506f.mockapi.io/api/v1/prospects';

export const ProspectList = (props) => {
  const {navigation} = props;
  // const prospects = dummyProspects;

  const [requestStatus, setRequestStatus] = useState('pending');
  const [prospects, setProspects] = useState([]);
  /*
   * initial -> belum request
   * pending | loading -> nunggu request
   * succeeded | fulfilled -> request berhasil
   * failed | rejected -> request gagal
   */

  useEffect(() => {
    async function getProspects() {
      setRequestStatus('pending');
      const response = await axios
        .get(API_URL)
        .then((res) => res)
        .catch((err) => err);

      if (response.status == 200) {
        setRequestStatus('succeeded');
        const mappedProspects = response.data.map((prospect) => {
          if (!prospect.car.id) {
            return {
              ...prospect,
              car: getRandomCar(),
            };
          } else {
            return prospect;
          }
        });
        console.log('mappedProspects', mappedProspects);
        setProspects(mappedProspects);
      } else {
        setRequestStatus('failed');
      }
    }

    getProspects();
  }, []);

  console.log('request status', requestStatus);

  return (
    <View style={styles.container}>
      <View style={styles.topButtonWrapper}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Cars')}
          style={styles.topButton}>
          <Text style={styles.textLg}>Cars</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.topButton}>
          <Text>Prospects</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.listContainer}>
        {requestStatus === 'pending' && <Text>Loading...</Text>}
        {requestStatus === 'succeeded' && (
          <FlatList
            data={prospects}
            renderItem={({item}) => <ProspectItem data={item} />}
            keyExtractor={(item) => String(item.id)}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
          />
        )}
        {requestStatus === 'failed' && <Text>Request Failed...</Text>}
      </View>
    </View>
  );
};

export default ProspectList;
