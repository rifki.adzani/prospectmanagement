import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AddProspect from '../screens/AddProspect.screen';
import ProspectList from '../screens/ProspectList.screen';
import CarList from '../screens/CarList.screen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainTab = (props) => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="ProspectList" component={ProspectList} />
      <Tab.Screen name="AddProspect" component={AddProspect} />
    </Tab.Navigator>
  );
};

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="MainTab" component={MainTab} />
        <Stack.Screen name="Cars" component={CarList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
