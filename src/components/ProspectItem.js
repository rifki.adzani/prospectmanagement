import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export const ProspectItem = ({data, onPress}) => {
  return (
    <TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          borderWidth: 1,
          borderColor: 'hsl(210, 5%, 70%)',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
          paddingVertical: 10,
          backgroundColor: 'hsl(0, 0%, 98%)',
          borderRadius: 5,
        }}>
        <View style={{flex: 1}}>
          <Text style={{fontSize: 18, color: 'hsl(0, 0%, 5%)', fontWeight: '900'}}>
            {data.name}
          </Text>
        </View>
        <View
          style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <View
            style={{
              backgroundColor: data.car.backgroundColor,
              maxWidth: 280,
              borderRadius: 2,
              borderColor: '#000',
              borderWidth: 2,
              paddingHorizontal: 10,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                color: data.car.fontColor,
                textAlign: 'center',
              }}>
              {data.car.name}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
